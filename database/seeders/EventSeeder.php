<?php

use App\Models\Event;
use Carbon\CarbonImmutable;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ['WEBINAR', 'ART', 'CHARITY','COMPETITION','EDUCATION','EDUCATION EXPO','PERFORMANCE','PHOTOGRAPHY','UNIVERSITY STUDENT','WORKSHOP'];
        
        $location['address'] = [
            'address' => 'Jl Batu Kuda No.12',
            'lat' => '-6.891709',
            'lat' => ' 107.647442',
        ];
        
        $dataEvent = [
            [
                'name'         => 'Poster Design Competition - Self love, Self independence Generasi Muda di Era Digital',
                'description'  => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'categories'   => json_encode($categories),
                'email'        => 'rivannurdin001@gmail.com',
                'short_link'   => 'event1',
                'privacy_type' => 1,
                'image'        => '/public/image/event/event1.jpg',
                'city_id'      => 1,
                'location'     => json_encode($location),
                'start_date'   => CarbonImmutable::parse('2020-08-30 10:30')->format('Y-m-d H:i:s'),
                'end_date'     => CarbonImmutable::parse('2020-08-30 13:00')->format('Y-m-d H:i:s'),
            ],
            [
                'name'         => 'SQL for Big Data Analysis',
                'description'  => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'categories'   => json_encode($categories),
                'email'        => 'rivannurdin001@gmail.com',
                'short_link'   => 'event2',
                'privacy_type' => 1,
                'image'        => '/public/image/event/event2.jpg',
                'city_id'      => 1,
                'location'     => json_encode($location),
                'start_date'   => CarbonImmutable::parse('2020-09-01 10:30')->format('Y-m-d H:i:s'),
                'end_date'     => CarbonImmutable::parse('2020-09-01 13:00')->format('Y-m-d H:i:s'),
            ],
            [
                'name'         => 'Webinar Affiliate Marketing',
                'description'  => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'categories'   => json_encode($categories),
                'email'        => 'rivannurdin001@gmail.com',
                'short_link'   => 'event3',
                'privacy_type' => 1,
                'image'        => '/public/image/event/event4.jpg',
                'city_id'      => 1,
                'location'     => json_encode($location),
                'start_date'   => CarbonImmutable::parse('2020-09-01 10:30')->format('Y-m-d H:i:s'),
                'end_date'     => CarbonImmutable::parse('2020-09-01 13:00')->format('Y-m-d H:i:s'),
            ],
        ];

        foreach ($dataEvent as $value) {
            $model = new Event();

            $model->name         = $value['name'];
            $model->description  = $value['description'];
            $model->categories   = $value['categories'];
            $model->email        = $value['email'];
            $model->short_link   = $value['short_link'];
            $model->privacy_type = $value['privacy_type'];
            $model->image        = $value['image'];
            $model->city_id      = $value['city_id'];
            $model->location     = $value['location'];
            $model->start_date   = $value['start_date'];
            $model->end_date     = $value['end_date'];

            $model->save();
        }
    }
}
