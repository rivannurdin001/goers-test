<?php

use App\Models\User;
use Carbon\CarbonImmutable;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            'id'         => 1,
            'first_name' => 'User 1',
            'last_name'  => 'User 1',
            'email'      => 'user1@mail.com',
            'password'   => Hash::make('12345'),
            'birthday'   => '1999-06-05',
            'avatar'     => null,
            'phone'      => null,
            'gender'     => 1
        ];
        
        User::insert($user);
    }
}
