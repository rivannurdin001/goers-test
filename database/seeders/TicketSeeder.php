<?php

use App\Models\Event;
use App\Models\Ticket;
use Carbon\CarbonImmutable;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class TicketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $description= [
            'terms_conditions'          => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            'price_picket_include'      => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            'price_picket__not_include' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
        ];
        
        $dataTicket = [
            [
                'event_id'    => 1,
                'type'        => 1,
                'name'        => 'DialectAct:Poster Design Competition - Self love dan Self independence Generasi Muda di Era Digital',
                'price'       => 150000,
                'quantity'    => 100,
                'start_date'  => CarbonImmutable::parse('2020-08-30 10:30')->format('Y-m-d H:i:s'),
                'end_date'    => CarbonImmutable::parse('2020-08-30 13:00')->format('Y-m-d H:i:s'),
                'description' => json_encode($description),
            ],
            [
                'event_id'    => 1,
                'type'        => 1,
                'name'        => 'DialectAct:Poster Design Competition - Self love dan Self independence Generasi Muda di Era Digital',
                'price'       => 150000,
                'quantity'    => 100,
                'start_date'  => CarbonImmutable::parse('2020-08-30 13:30')->format('Y-m-d H:i:s'),
                'end_date'    => CarbonImmutable::parse('2020-08-30 15:00')->format('Y-m-d H:i:s'),
                'description' => json_encode($description),
            ],
            [
                'event_id'    => 2,
                'type'        => 1,
                'name'        => 'ClassON - SQL for Big Data Analysis',
                'price'       => 100000,
                'quantity'    => 50,
                'start_date'  => CarbonImmutable::parse('2020-08-30 13:30')->format('Y-m-d H:i:s'),
                'end_date'    => CarbonImmutable::parse('2020-08-30 15:00')->format('Y-m-d H:i:s'),
                'description' => json_encode($description),
            ],
            [
                'event_id'    => 2,
                'type'        => 1,
                'name'        => 'ClassON - SQL for Big Data Analysis',
                'price'       => 100000,
                'quantity'    => 50,
                'start_date'  => CarbonImmutable::parse('2020-09-01 13:30')->format('Y-m-d H:i:s'),
                'end_date'    => CarbonImmutable::parse('2020-09-01 15:00')->format('Y-m-d H:i:s'),
                'description' => json_encode($description),
            ],
            [
                'event_id'    => 2,
                'type'        => 1,
                'name'        => 'ClassON - SQL for Big Data Analysis',
                'price'       => 100000,
                'quantity'    => 50,
                'start_date'  => CarbonImmutable::parse('2020-09-02 13:30')->format('Y-m-d H:i:s'),
                'end_date'    => CarbonImmutable::parse('2020-09-02 15:00')->format('Y-m-d H:i:s'),
                'description' => json_encode($description),
            ],
            [
                'event_id'    => 3,
                'type'        => 2,
                'name'        => 'Webinar 28 Ags 21',
                'price'       => 0,
                'quantity'    => 50,
                'start_date'  => CarbonImmutable::parse('2020-09-02 13:30')->format('Y-m-d H:i:s'),
                'end_date'    => CarbonImmutable::parse('2020-09-02 15:00')->format('Y-m-d H:i:s'),
                'description' => json_encode($description),
            ],
        ];

        foreach ($dataTicket as $value) {
            $model = new Ticket();

            $model->event_id     = $value['event_id'];
            $model->type         = $value['type'];
            $model->name         = $value['name'];
            $model->price        = $value['price'];
            $model->quantity     = $value['quantity'];
            $model->start_date   = $value['start_date'];
            $model->end_date     = $value['end_date'];
            $model->description  = $value['description'];

            $model->save();
        }
    }
}
