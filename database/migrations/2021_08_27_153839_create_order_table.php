<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->integer('user_id')->nullable();
            $table->string('email');
            $table->string('phone');
            $table->string('first_name');
            $table->string('last_name');
            $table->integer('gender')->comment('1:male 2:female');
            $table->string('voucher_code')->nullable();
            $table->float('discount')->nullable();
            $table->float('service_fee')->nullable();
            $table->float('tax')->nullable();
            $table->decimal('subtotal');
            $table->decimal('grand_total');
            $table->integer('payment_method_id');
            $table->dateTime('payment_deadline');
            $table->integer('status')->comment('1:Waiting For Payment, 2: Payment Complete, 3: Canceled');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
