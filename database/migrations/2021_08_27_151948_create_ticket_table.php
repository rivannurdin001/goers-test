<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('event_id')->unsigned()->index();
            $table->integer('type')->comment('1:not free, 2:free');
            $table->string('name');
            $table->decimal('price');
            $table->integer('quantity');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->json('description')->nullable()->comment('terms_conditions, price_picket_include, price_picket__not_include');
            $table->timestamps();

            $table->foreign('event_id')->references('id')->on('event');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket');
    }
}
