<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'v1'], function($router) {

    $router->group([ 'prefix' => 'user', 'namespace' => 'User' ], function($router) {

        $router->group(['prefix' => 'auth', 'namespace' => 'Auth'], function($router) {
            $router->post('login',  'LoginController');
            $router->post('register', 'RegisterController');
        });

        $router->group(['prefix' => 'event', 'namespace' => 'Event'], function ($router) {
            $router->get('list',    'ListController');
            $router->get('detail/{shortLink:[0-9A-Za-z]+}',  'DetailController');
        });

        $router->group(['prefix' => 'order', 'namespace' => 'Order'], function ($router) {
            $router->post('create',                     'CreateController');
            $router->get('list',                        'ListController');
            $router->get('detail/{id:[0-9A-Za-z]+}',  'DetailController');
        });

        $router->group(['prefix' => 'payment', 'namespace' => 'Payment'], function ($router) {
            $router->post('create/{orderId:[0-9A-Za-z]+}',   'CreateController');
            $router->post('detail/{orderId:[0-9A-Za-z]+}',   'DetailController');
            $router->post('change_status/{orderId:[0-9A-Za-z]+}', 'ChangStatusController');
        });
    });

});
