<?php

use App\Models\Order;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Http\File;
use Carbon\CarbonImmutable;
use Carbon\CarbonInterface;
use Carbon\CarbonPeriod;
use Illuminate\Pagination\LengthAwarePaginator;
use Intervention\Image\Facades\Image;

if (!function_exists('rupiah_format')) {
    function rupiah_format($number)
    {
        return number_format($number, 0, ',', '.');
    }
}

if (!function_exists('metaPagination')) {
    function metaPagination($data)
    {
        if (!$data) {
            $data = new LengthAwarePaginator(0, 0, 10);
        }

        return [
            'pagination' => [
                'total'         => $data->total(),
                'count'         => $data->count(),
                'per_page'      => $data->perPage(),
                'current_page'  => $data->currentPage(),
                'total_pages'   => $data->lastPage(),
                'has_more_page' => $data->hasMorePages(),
            ],
        ];
    }
}

if (!function_exists('public_path')) {
    /**
     * Return the path to public dir
     *
     * @param null $path
     *
     * @return string
     */
    function public_path($path = null)
    {
        return rtrim(app()->basePath('public/' . $path), '/');
    }
}

if (!function_exists('saveFile')) {
    function saveFile($file, $path, $filename = '')
    {
        $filename    = $filename ? $filename : $file->getClientOriginalName();
        $updloadPath = public_path($path);

        if (!file_exists($updloadPath)) {
            mkdir($updloadPath, 0775, true);
        }

        $file->move($updloadPath, $filename);

        return "{$path}/{$filename}";
    }
}

if (!function_exists('setFileUrl')) {
    function setFileUrl($file)
    {
        if (!$file || !file_exists(public_path() . '/' . $file)) {
            return null;
        }

        return env('APP_URL') . '/' . $file;
    }
}

if (!function_exists('storeFile')) {
    function storeFile($path, $file)
    {
        return ($file && $path) ? Storage::disk('upload')->putFile($path, $file) : false;
    }
}

if (!function_exists('storeFileUrl')) {
    function storeFileUrl($path, $file, $fileName = null)
    {
        if ($file && $path) {
            $temp = 'tmp/' . Str::random(40);

            if (is_object($file)) {
                Storage::put($temp, file_get_contents($file));
            } elseif (is_string($file)) {
                $base64 = explode(',', $file);
                $base64 = $base64[count($base64) - 1];
                Storage::put($temp, base64_decode($base64));
            }

            $result = $fileName ? saveFile(new File(storage_path('app/' . $temp)), $path, $fileName) : storeFile($path, new File(storage_path('app/' . $temp)));

            Storage::delete($temp);
            return $result;
        }
    }
}

if (!function_exists('storeFile64')) {
    function storeFile64($path, $file)
    {
        $base64 = explode(',', $file)[1];
        if ($file && $path) {
            $temp = 'tmp/' . Str::random(40);
            Storage::put($temp, base64_decode($base64));
            $result = storeFile($path, new File(storage_path('app/' . $temp)));
            Storage::delete($temp);
            return $result;
        }
    }
}

if (!function_exists('deleteFile')) {
    function deleteFile($path)
    {
        return ($path) ? Storage::disk('upload')->delete($path) : false;
    }
}

if (!function_exists('generateOrderCode')) {
    function generateOrderCode()
    {
        $order = Order::orderby('created_at', 'desc')->first();

        if (!$order || substr($order->code, 8, 12) == "") {
            $code = str_pad(1, 4, '0', STR_PAD_LEFT);
            // dd($code);
        } else {
            $id = (int) substr($order->code, 8, 12);
            $code = str_pad(++$id, 4, '0', STR_PAD_LEFT);
        }

        return date('Ymd') . $code;
    }
}