<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'event';

    // protected $dates = ['deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'location' => 'array',
        'categories' => 'array'
    ];

    /**
     * Relationship.
     *
     */

    public function Order()
    {
        return $this->hasMany(Order::class, 'order_id', 'id');
    }

    public function Ticket()
    {
        return $this->hasMany(Ticket::class, 'event_id', 'id');
    }
}
