<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    // payment status
    const WAITING_PAYMENT  = 1;
    const PAYMENT_COMPLETE = 2;
    const ORDER_CANCELED   = 3;

    // Midtrans Transaction Status 

    const PENDING              = 'pending';
    const CAPTURE              = 'capture';
    const SETTLEMENT           = 'settlement';
    const DENY                 = 'deny';
    const CANCEL               = 'cancel';
    const EXPIRE               = 'expire';
    const FAILURE              = 'failure';
    const REFUND               = 'refund';
    const PARTIAL_REFUND       = 'partial_refund';
    const PARTIAL_CAHARGERBACK = 'partial_chargeback';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order';

    // protected $dates = ['deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'categories' => 'array',
        'location' => 'array'
    ];

    /**
     * Relationship.
     *
     */

    public static function statusDropdown()
    {
        return [
            self::WAITING_PAYMENT   => 'Waiting For Payment',
            self::PAYMENT_COMPLETE   => 'Payment Complete',
            self::ORDER_CANCELED   => 'Canceled'
        ];
    }

    public function getStatus()
    {
        return $this->statusDropdown()[$this->status];
    }

    public function OrderDetail()
    {
        return $this->hasMany(OrderDetail::class, 'order_id', 'id');
    }
}
