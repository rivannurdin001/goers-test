<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'invoice';

    // protected $dates = ['deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'actions' => 'array'
    ];

    /**
     * Relationship.
     *
     */

    public function Ticket()
    {
        return $this->hasMany(Ticket::class, 'event_id', 'id')->orderBy('ticket.start_date');
    }
}
