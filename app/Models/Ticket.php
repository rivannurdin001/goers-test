<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    // ticket type
    const NOTFREE = 1;
    const FREE    = 2;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ticket';

    // protected $dates = ['deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'description' => 'array'
    ];

    /**
     * Relationship.
     *
     */

    public static function TypeDropdown()
    {
        return [
            self::NOTFREE   => 'No Free',
            self::FREE   => 'Free',
        ];
    }

    public function getType()
    {
        return $this->TypeDropdown()[$this->type];
    }

    public function Event()
    {
        return $this->belongsTo(Event::class, 'event_id', 'id');
    }
}
