<?php

namespace App\Http\Middleware;

use App\Models\Admin;
use Closure;
use Illuminate\Auth\Access\AuthorizationException;

class AccessRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, int $role = null)
    {
        if ($request->user()->roles_id === $role) {
            return $next($request);
        }

        throw new AuthorizationException();
    }
}
