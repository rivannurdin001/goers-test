<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    protected function trueResponse($message, $data = null, $meta = null)
    {
        $data = $this->format($data);
        $meta = $this->format($meta);

        return response()->json([
            'status'  => true,
            'message' => $message,
            'data'    => $data,
            'meta'    => $meta,
            'error'   => []
        ], Response::HTTP_OK);
    }

    protected function falseResponse($message, $errors = null, $list = false)
    {
        $errors = $this->format($errors);

        $data = (object)[];
        if ($list) {
            $data = [];
        }

        return response()->json([
            'status'  => false,
            'message' => $message,
            'data'    => $data,
            'meta'    => (object)[],
            'error'   => $errors
        ], Response::HTTP_OK);
    }

    protected function format($var)
    {
        if (is_array($var) && sizeof($var) === 0) [];
        elseif (!$var) $var = (object)[];
        return $var;
    }
}
