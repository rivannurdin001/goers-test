<?php

namespace App\Http\Controllers\User\Order;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Carbon\CarbonImmutable;

class DetailController extends Controller
{
    public function __invoke($id)
    {
        $model = Order::where('id', $id)
            ->with('OrderDetail')
            ->first();

        if (!$model) {
            return $this->falseResponse('Data Not Found');
        }

        return $this->trueResponse('Detail Order', $this->transformer($model));
    }

    private function transformer($model)
    {
        $ticketDetail = [];
        foreach ($model->OrderDetail as $orderDetail) {
            $eventName = $orderDetail->Ticket->Event->name;

            $ticketDetail[] = [
                'name'       => $orderDetail->Ticket->name,
                'quantity'   => $orderDetail->quantity,
                'start_date' => CarbonImmutable::parse($orderDetail->Ticket->start_date)->format('d M Y, H:i'),
                'end_date' => CarbonImmutable::parse($orderDetail->Ticket->end_date)->format('d M Y, H:i')
            ];
        }

        $result = [
            'id'                    => $model->id,
            'event_name'            => $eventName,
            'payment_deadline_time' => CarbonImmutable::parse($model->payment_deadline)->format('H:i'),
            'payment_deadline_date' => CarbonImmutable::parse($model->payment_deadline)->format('d M Y'),
            'order_number'          => $model->code,
            'buyer_email'           => $model->email,
            'order_detail'          => $ticketDetail,
            'sub_total'             => 'IDR '. rupiah_format($model->subtotal),
            'service_fee'           => 'IDR '. rupiah_format($model->service_fee),
            'discount'              => $model->discount.'%',
            'grand_total'           => 'IDR '.rupiah_format($model->grand_total),
            'payment_method_id'     => $model->payment_method_id,
            'status'                => $model->getStatus(),
        ];

        return $result;
    }
}
