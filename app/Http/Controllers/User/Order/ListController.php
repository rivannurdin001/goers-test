<?php

namespace App\Http\Controllers\User\Order;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Order;
use App\Models\Ticket;
use App\Models\User;
use Carbon\CarbonImmutable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ListController extends Controller
{
    public function __invoke()
    {
        $user = Auth::guard()->user();
        $models = Order::where('user_id', $user->id)
            ->orderBy('created_at', 'DESC')->paginate(15);

        return $this->trueResponse('List Order', $this->transformer($models), metaPagination($models));
    }

    private function transformer($models)
    {
        $result = [];
        foreach ($models as $model) {
            foreach ($model->OrderDetail as $orderDetail) {
                $eventName = $orderDetail->Ticket->Event->name;
            }

            $result[] = [
                'id'          => $model->id,
                'code'        => $model->code,
                'event_name'  => $eventName,
                'status'      => $model->getStatus(),
                'grand_total' => 'IDR '.rupiah_format($model->grand_total),
            ];
        }

        return $result;
    }
}
