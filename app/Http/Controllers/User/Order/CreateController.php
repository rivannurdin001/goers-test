<?php

namespace App\Http\Controllers\User\Order;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Ticket;
use App\Models\User;
use Carbon\CarbonImmutable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CreateController extends Controller
{
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'email'             => 'required|email',
            'first_name'        => 'required',
            'last_name'         => 'required',
            'phone'             => 'required',
            'gender'            => 'required',
            'payment_method_id' => 'required',
            'ticket_id.0'      => 'required',
            'quantity.0'       => 'required',
        ]);

        $user = Auth::guard()->user();

        $model = new Order();

        DB::transaction(function () use ($request, $model, $user) {

            $grandTotal = 0;
            $subTotal   = 0;

            if ($request->service_fee) {
                $model->service_fee = $request->service_fee;
                $grandTotal +=  $request->service_fee;
            }

            
            if ($request->tax) {
                $model->tax = $request->tax;
                $grandTotal +=  $request->tax;
            }

            $subTotal = 0;

            $model->code              = generateOrderCode();
            $model->user_id           = ($user) ? $user->id : null;
            $model->email             = $request->email;
            $model->first_name        = $request->first_name;
            $model->last_name         = $request->last_name;
            $model->phone             = $request->phone;
            $model->gender            = $request->gender;
            $model->payment_method_id = $request->payment_method_id;
            $model->status            = Order::WAITING_PAYMENT;
            $model->subtotal          = $subTotal;
            $model->grand_total       = $grandTotal;
            $model->payment_deadline  = CarbonImmutable::now()->addMinutes(90);

            $model->save();

            $subTotal = [];
            foreach ($request->ticket_id as $key => $ticketId) {
                $ticket = Ticket::where('id', $ticketId)->first();
                $total = $ticket->price * $request->quantity[$key];
                
                $subTotal[] = $total;

                $orderDetail = new OrderDetail();

                $orderDetail->order_id  = $model->id;
                $orderDetail->ticket_id = $ticketId;
                $orderDetail->quantity  = $request->quantity[$key];
                $orderDetail->total     = $total;

                $orderDetail->save();
            }

            $grandTotal += array_sum($subTotal);

            $order = Order::where('id', $model->id)->first();

            if ($request->voucher_code){
                $order->voucher_code = $request->voucher_code;
                $order->discount     = $request->discount;

                $dicount = ($grandTotal * $request->discount / 100);
                $grandTotal -= $dicount;
            }

            $order->subtotal    = array_sum($subTotal);
            $order->grand_total = $grandTotal;

            $order->save();

        }, env("DB_T_RETRY", 3));

        $data['data'] = [
            'name'             => $model->first_name.' '.$model->last_name,
            'email'            => $model->email,
            'phone'            => $model->phone,
            'grand_total'      => $model->grand_total,
            'payment_deadline' => CarbonImmutable::parse($model->payment_deadline)->format('Y-m-d H:i:s')
        ];

        return $this->falseResponse('Order Success', $data);
    }
}
