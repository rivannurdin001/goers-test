<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\CarbonImmutable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);

        $credentials = [
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        ];
        
        if ( !$user = User::where('email', $request->email)->first() ){
            return $this->falseResponse('Account Not Found');
        }

        if (!$token = Auth::guard()->attempt($credentials)) {
            return $this->falseResponse('Invalid email or password', [
                [
                    'attribute' => '_global',
                    'text'      => 'Invalid email or password'
                ]
            ]);
        }

        return $this->trueResponse('Login sukses', [
            'access_token' => $token,
            'token_type'   => 'bearer',
            'role'         => 'user',
        ]);
    }
}
