<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\CarbonImmutable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function __invoke(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'username' => 'required',
            'password' => 'required',
            'role_id' => 'required',
        ]);
        
        if ( User::where('username', $request->username)->first() ){
            return $this->falseResponse('Username Has Been Used');
        }

        $model = new User();

        $model->name = $request->name;
        $model->username = $request->username;
        $model->password = Hash::make($request->password);
        $model->role_id = $request->role_id;
        
        $model->save();

        return $this->trueResponse('Register sukses');
    }
}
