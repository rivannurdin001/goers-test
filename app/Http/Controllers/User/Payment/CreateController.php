<?php

namespace App\Http\Controllers\User\Payment;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Order;
use Illuminate\Http\Request;
\Midtrans\Config::$serverKey = 'SB-Mid-server-qRZ88-OH99lxMsj7dac1jr_q';

class CreateController extends Controller
{
    public function __invoke($orderId)
    {
        if(!$order = Order::where('code', $orderId)->first()){
            return $this->falseResponse('Order Not Found');
        }

        $params = array(
            'transaction_details' => array(
                'order_id' => $order->code,
                'gross_amount' => $order->grand_total,
            ),
            'payment_type' => 'gopay',
        );

        $response = \Midtrans\CoreApi::charge($params);

        return $this->trueResponse($response);
    }
}
