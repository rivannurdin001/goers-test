<?php

namespace App\Http\Controllers\User\Payment;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Order;
use Carbon\CarbonImmutable;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

\Midtrans\Config::$serverKey = 'SB-Mid-server-qRZ88-OH99lxMsj7dac1jr_q';

class DetailController extends Controller
{
    public function __invoke($orderId)
    {
        if(!$model = Order::where('code', $orderId)->first()){
            return $this->falseResponse('Order Not Found');
        }

        $status = \Midtrans\Transaction::status($orderId);

        $ticketDetail = [];
        foreach ($model->OrderDetail as $orderDetail) {
            $eventName = $orderDetail->Ticket->Event->name;

            $ticketDetail[] = [
                'name'       => $orderDetail->Ticket->name,
                'quantity'   => $orderDetail->quantity,
                'start_date' => CarbonImmutable::parse($orderDetail->Ticket->start_date)->format('d M Y, H:i'),
                'end_date'   => CarbonImmutable::parse($orderDetail->Ticket->end_date)->format('d M Y, H:i')
            ];
        }

        $model = [
                'id'                    => $model->id,
                'event_name'            => $eventName,
                'payment_deadline_time' => CarbonImmutable::parse($model->payment_deadline)->format('H:i'),
                'payment_deadline_date' => CarbonImmutable::parse($model->payment_deadline)->format('d M Y'),
                'order_number'          => $model->code,
                'buyer_email'           => $model->email,
                'order_detail'          => $ticketDetail,
                'sub_total'             => 'IDR '. rupiah_format($model->subtotal),
                'service_fee'           => 'IDR '. rupiah_format($model->service_fee),
                'discount'              => $model->discount.'%',
                'grand_total'           => 'IDR '.rupiah_format($model->grand_total),
                'payment_method_id'     => $model->payment_method_id,
                'status'                => $model->getStatus(),
        ];

        $data['order'] = $model;
        $data['transaction_status'] = $status;

        return $this->trueResponse('data', $data);
    }
}
