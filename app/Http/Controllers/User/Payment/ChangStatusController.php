<?php

namespace App\Http\Controllers\User\Payment;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\Order;
use Carbon\CarbonImmutable;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

\Midtrans\Config::$serverKey = 'SB-Mid-server-qRZ88-OH99lxMsj7dac1jr_q';

class ChangStatusController extends Controller
{
    public function __invoke($orderId)
    {
        if(!$model = Order::where('code', $orderId)->first()){
            return $this->falseResponse('Order Not Found');
        }

        $status = \Midtrans\Transaction::status($orderId);

        if ($status->transaction_status == Order::SETTLEMENT && $status->transaction_status != Order::PENDING ) 
        {
            $model->status = Order::PAYMENT_COMPLETE;
        }elseif($status->transaction_status == Order::CANCEL || $status->transaction_status == Order::EXPIRE )
        {
            $model->status = Order::ORDER_CANCELED;
        }

        $model->save();

        return $this->trueResponse('Success Change Status');
    }
}
