<?php

namespace App\Http\Controllers\User\Event;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Ticket;
use App\Models\User;
use Carbon\CarbonImmutable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DetailController extends Controller
{
    public function __invoke($shortLink)
    {
        $model = Event::where('short_link', $shortLink)->first();

        return $this->trueResponse('Detail Event', $this->transformer($model));
    }

    private function transformer($model)
    {
        $tickets = [];

        foreach ($model->Ticket as $value) {
            $tickets[] = [
                'id'          => $value->id,
                'name'        => $value->name,
                'price'       => rupiah_format($value->price),
                'quantity'    => $value->quantity,
                'start_date'  => CarbonImmutable::parse($value->start_date)->format('d M Y'),
                'start_time'  => CarbonImmutable::parse($value->start_date)->format('H:i'),
                'end_date'    => CarbonImmutable::parse($value->end_date)->format('d M Y'),
                'end_time'    => CarbonImmutable::parse($value->end_date)->format('H:i'),
                'description' => $value->description
            ];
        }

        $result = [
            'id'          => $model->id,
            'name'        => $model->name,
            'image'       => setFileUrl($model->image),
            'short_link'  => $model->short_link,
            'start_date'  => CarbonImmutable::parse($model->start_date)->format('d M Y'),
            'start_time'  => CarbonImmutable::parse($model->start_date)->format('H:i'),
            'country'     => 'Indonesia',                                                   // dummy
            'city'        => 'Bandung',
            'location'    => $model->location,
            'categories'  => $model->categories,
            'description' => $model->description,
            'ticket'      => $tickets
        ];

        return $result;
    }
}
