<?php

namespace App\Http\Controllers\User\Event;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Ticket;
use App\Models\User;
use Carbon\CarbonImmutable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ListController extends Controller
{
    public function __invoke(Request $request)
    {
        $models = Event::orderBy('id', 'ASC')->paginate(15);

        return $this->trueResponse('List Event', $this->transformer($models), metaPagination($models));
    }

    private function transformer($models)
    {
        $result = [];
        foreach ($models as $model) {

            // $price = 'Free';

            if ($model->Ticket) {
                foreach ($model->Ticket as $value) {
                    if ($value->type == Ticket::NOTFREE) {
                        $price = (int) $value['price'];
                    }
    
                    $result[] = [
                        'id'         => $model->id,
                        'name'       => $model->name,
                        'image'      => setFileUrl($model->image),
                        'short_link' => $model->short_link,
                        'start_date' => CarbonImmutable::parse($model->start_date)->format('d M Y'),
                        'start_time' => CarbonImmutable::parse($model->start_date)->format('H:i'),
                        'country'    => 'Indonesia',                                                   // dummy
                        'city'       => 'Bandung',
                        'price'      => 'From IDR '.rupiah_format($price),
                    ];
                }
            }
        }

        return $result;
    }
}
