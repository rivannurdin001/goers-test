<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $e)
    {
        $code = Response::HTTP_INTERNAL_SERVER_ERROR;

        if ($e instanceof HttpResponseException) {
            if (env('APP_DEBUG')) return response()->json([
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'code' => $e->getCode(),
            ], $code);
        } elseif ($e instanceof MethodNotAllowedHttpException) {
            $code = Response::HTTP_METHOD_NOT_ALLOWED;
        } elseif ($e instanceof NotFoundHttpException || $e instanceof ModelNotFoundException) {
            $code = Response::HTTP_NOT_FOUND;
        } elseif ($e instanceof AuthorizationException) {
            $code = Response::HTTP_FORBIDDEN;
        } elseif ($e instanceof AuthenticationException ) {
            $code = Response::HTTP_UNAUTHORIZED;
        } elseif ($e instanceof ValidationException) {
            $errorBag = [];
            $message  = $e->getMessage();
            foreach($e->errors() as $key => $value) $errorBag[] = [ 'attribute' => $key, 'text' => $value[0]];
            if (!empty($errorBag)) { $message = $errorBag[0]['text']; }
            return response()->json([
                'status'  => false,
                'message' => $message,
                'data'    => (object)[],
                'meta'    => (object)[],
                'error'   => $errorBag
            ], Response::HTTP_OK);
        } else {
            if (env('APP_DEBUG')) return response()->json([
                'message' => $e->getMessage(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'code' => $e->getCode(),
            ], $code);
        }

        return response('', $code);

        // return parent::render($request, $exception);
    }
}
