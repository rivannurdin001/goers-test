<?php

return [
    'required_claims' => [
        'iss',
        'iat',
        'nbf',
        'sub',
        'jti',
    ],

    'lock_subject' => true,

    'ttl' => env('JWT_TTL', 60),
];