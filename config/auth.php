<?php

return [
    'defaults' => [
        'guard'     => env('AUTH_GUARD', 'user'),
        'passwords' => 'user',
    ],

    'guards' => [
        'user' => [
            'driver'   => 'jwt',
            'provider' => 'user',
        ],
        'admin' => [
            'driver'   => 'jwt',
            'provider' => 'admin',
        ]
    ],

    'providers' => [
        'user' => [
            'driver' => 'eloquent',
            'model'  => App\Models\User::class,
        ],
        'admin' => [
            'driver' => 'eloquent',
            'model'  => App\Models\Admin::class,
        ]
    ],

    'passwords' => [
        'user' => [
            'provider' => 'user',
            'table'    => 'password_resets',
            'expire'   => 60,
        ],
        'admin' => [
            'provider' => 'admin',
            'table'    => 'password_resets',
            'expire'   => 60,
        ],
    ],
];
